using System;
using System.Collections.Generic;

namespace LV2_RPPOON
{
    class Program
    { 
	
		//Zadatak 1
		/*static void Print(IList<int> results)
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(results[i].ToString());
            }
        }
		static void Main(string[] args)
        {
            DiceRoller rolls = new DiceRoller();

            for (int i = 0; i < 20; i++)
            {
                rolls.InsertDie(new Die(6));
            }

            rolls.RollAllDice();
            IList<int> results = rolls.GetRollingResults();
            Print(results);
		}
		*/
		
		//Zadatak 2
		/*static void Print(IList<int> results)
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(results[i].ToString());
            }
        }
		static void Main(string[] args)
        {
            DiceRoller rolls = new DiceRoller();
            Random random = new Random();

            for (int i = 0; i < 20; i++)
            {
                rolls.InsertDie(new Die(6, random));
            }

            rolls.RollAllDice();
            IList<int> results = rolls.GetRollingResults();
            Print(results);
		}
		*/
		
		
		//Zadatak 3
		/*static void Print(IList<int> results)
        {
            for (int i = 0; i < 20; i++)
            {
                Console.WriteLine(results[i].ToString());
            }
        }
		static void Main(string[] args)
        {
            DiceRoller rolls = new DiceRoller();

            for (int i = 0; i < 20; i++)
            {
                rolls.InsertDie(new Die(6));
            }

            rolls.RollAllDice();
            IList<int> results = rolls.GetRollingResults();
            Print(results);
		}
		*/
		
		
		//Zadatak 4
		/*
		static void Main(string[] args)
        {
			DiceRoller diceRoller = new DiceRoller();
            ILogger logger = new ConsoleLogger();

            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }
            diceRoller.RollAllDice();

            diceRoller.SetLogger(logger);
			diceRoller.LogRollingResults();
		}
		*/
		
		//Zadatak 5
		/*
		static void Main(string[] args)
        {
            DiceRoller diceRoller = new DiceRoller();
            ConsoleLogger consoleLogger = new ConsoleLogger();

            for (int i = 0; i < 20; i++)
            {
                diceRoller.InsertDie(new Die(6));
            }

            diceRoller.RollAllDice();
            consoleLogger.Log(diceRoller);
        }
		*/
		
		//Zadatak 6
		/*static void Main(string[] args)
        {
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            ClosedDiceRoller closedDiceRoller = new ClosedDiceRoller(20, 6);
            
            for(int i = 0; i < 5; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(6));
                flexibleDiceRoller.InsertDie(new Die(7));
                flexibleDiceRoller.InsertDie(new Die(8));
                flexibleDiceRoller.InsertDie(new Die(9));
            }

            flexibleDiceRoller.RollAllDice();
            closedDiceRoller.RollAllDice();

            ILogger consoleLogger = new ConsoleLogger();

            consoleLogger.Log(flexibleDiceRoller);
            consoleLogger.Log(closedDiceRoller);
            
        }
		*/
		
		
		//Zadatak 7
        static void Main(string[] args)
        {
            FlexibleDiceRoller flexibleDiceRoller = new FlexibleDiceRoller();
            ClosedDiceRoller closedDiceRoller = new ClosedDiceRoller(20, 6);
            
            for(int i = 0; i < 5; i++)
            {
                flexibleDiceRoller.InsertDie(new Die(6));
                flexibleDiceRoller.InsertDie(new Die(7));
                flexibleDiceRoller.InsertDie(new Die(8));
                flexibleDiceRoller.InsertDie(new Die(9));
            }

            flexibleDiceRoller.RollAllDice();
            closedDiceRoller.RollAllDice();

            ILogger consoleLogger = new ConsoleLogger();

            consoleLogger.Log(flexibleDiceRoller);
            consoleLogger.Log(closedDiceRoller);

            flexibleDiceRoller.RemoveSpecificDice(9);

            flexibleDiceRoller.RollAllDice();

            consoleLogger.Log(flexibleDiceRoller);
            
        }
    }
}