using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{
    class Die
    {
		//Zadatak1
		
		/*private int numberOfSides;
		private Random randomGenerator;
		public Die(int numberOfSides)
		{
			this.numberOfSides = numberOfSides;
			this.randomGenerator = new Random();
		}
		public int Roll()
		{
			return this.randomGenerator.Next(1, numberOfSides + 1);
		}
		*/
		
		
		//Zadatak2
		
		/*private int numberOfSides;
		private Random randomGenerator;
		public Die(int numberOfSides, Random randomGenerator)
		{
			this.numberOfSides = numberOfSides;
			this.randomGenerator = randomGenerator;
		}
		public int Roll()
		{
			return this.randomGenerator.Next(1, numberOfSides + 1);
		}
		*/
		
		
		//Zadatak3
		
		/*private int numberOfSides;
		private RandomGenerator randomGenerator;
		public Die(int numberOfSides)
		{
			this.numberOfSides = numberOfSides;
			this.randomGenerator = RandomGenerator.GetInstance();
		}
		public int Roll()
		{
			return this.randomGenerator.NextInt(1, numberOfSides + 1);
		}
		*/
		
		//Zadatak 4,5,6,7
		
        private int numberOfSides;
        private RandomGenerator randomGenerator;
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = RandomGenerator.GetInstance();
        }
        public int Roll()
        {
            int rolledNumber = randomGenerator.NextInt(1, numberOfSides + 1);
            return rolledNumber;
        }
        public int NumberOfSides
        {
            get { return this.numberOfSides; } 
        }
    }
}