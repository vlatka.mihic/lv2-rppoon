using System;
using System.Collections.Generic;
using System.Text;

namespace LV2_RPPOON
{

	//Zadatak 1, 2, 3
			
	/*
	class DiceRoller
		{
			private List<Die> dice;
			private List<int> resultForEachRoll;
	  
			public DiceRoller()
			{
				this.dice = new List<Die>();
				this.resultForEachRoll = new List<int>();
			}

			public void InsertDie(Die die)
			{
				dice.Add(die);
			}
			public void RollAllDice()
			{
				this.resultForEachRoll.Clear();
				foreach (Die die in dice)
				{
					this.resultForEachRoll.Add(die.Roll());
				}
			}
			public IList<int> GetRollingResults()
			{
				return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
			   this.resultForEachRoll
			   );
			}
			public int DiceCount
			{
			    get { return dice.Count; }
			}
		}
	*/
			
			
	//Zadatak 4, 
	
	/*
	class DiceRoller
		{
			private List<Die> dice;
			private List<int> resultForEachRoll;
			private ILogger logger;
	  
			public DiceRoller()
			{
				this.dice = new List<Die>();
				this.resultForEachRoll = new List<int>();
				this.logger = new ConsoleLogger();
			}
			
			public DiceRoller(ILogger logger)
			{
				this.dice = new List<Die>();
				this.resultForEachRoll = new List<int>();
				this.logger = logger;
			}

			public void InsertDie(Die die)
			{
				dice.Add(die);
			}
			public void RollAllDice()
			{
				this.resultForEachRoll.Clear();
				foreach (Die die in dice)
				{
					this.resultForEachRoll.Add(die.Roll());
				}
			}
			public IList<int> GetRollingResults()
			{
				return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
			   this.resultForEachRoll
			   );
			}
			public void SetLogger(ILogger logger)
			{
				this.logger = logger;
			}
			public int DiceCount
			{
			    get { return dice.Count; }
			}
			public void LogRollingResults()
			{
				foreach(int result in this.resultForEachRoll)
				{
					logger.Log(result.ToString());
				}
			}
	*/
			
			
	//Zadatak 5
	
	class DiceRoller: ILogable
		{
			private List<Die> dice;
			private List<int> resultForEachRoll;
	  
			public DiceRoller()
			{
				this.dice = new List<Die>();
				this.resultForEachRoll = new List<int>();
			}
			public void InsertDie(Die die)
			{
				dice.Add(die);
			}
			public void RollAllDice()
			{
				this.resultForEachRoll.Clear();
				foreach (Die die in dice)
				{
					this.resultForEachRoll.Add(die.Roll());
				}
			}
			public IList<int> GetRollingResults()
			{
				return new System.Collections.ObjectModel.ReadOnlyCollection<int>(
			   this.resultForEachRoll
			   );
			}
			public int DiceCount
			{
			    get { return dice.Count; }
			}
			public string GetStringRepresentation()
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (int result in this.resultForEachRoll)
				{
					stringBuilder.Append(result.ToString()).Append("\n");
				}
				return stringBuilder.ToString();
			}
		}
}